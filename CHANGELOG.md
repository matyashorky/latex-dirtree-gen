# CHANGELOG

## 0.4.1

- Small fix for the ignore flag

## 0.4.0

- Files starting with a dot (".") are hidden by default, show them by using `--include-hidden`
- README has been updated to improve readability

## 0.3.0

- Added `--ignore` parameter

## 0.2.1

- An attempt to fix broken PyPI package

## 0.2.0

- `--dots` parameter

## 0.1.1

- Escape underscores

## 0.1.0

- Initial release
