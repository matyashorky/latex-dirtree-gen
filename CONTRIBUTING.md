# CONTRIBUTING

If you encounter any problems with the package, or if you'd like to request some additional functionality, [file an issue](https://gitlab.com/matyashorky/latex-dirtree-gen/-/issues). I'll be happy to help.

PRs will be accepted, they only have to follow some basic rules (`black` formatted with no `flake8` issues; `pre-commit` is your friend). Please, open an issue before submitting a PR: it's just to prevent PRs with code changes I disagree with (such as breaking changes with no good reason).
